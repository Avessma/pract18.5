﻿#include <iostream>
#include <string>



 
class Player {
public:

	std::string Name; 
	int Score;
	Player* arr;

	Player() {
		Score = 0;
		Name = "default";
	}

	void EnterPlayers(Player* arr, int NumberOfPlayers) {  //В скобочках пишем то, что потом в main заменим на локальные значения
		for (int i = 0; i < NumberOfPlayers; i++)        /*Вызывается цикл for с командами ввода информации на игроков, 
			                                            который выведется столько раз, сколько игроков мы указали в начале, и введенные данные сохранятся в массив*/
		{
			std::cout << "Name: ";
			std::cin >> arr[i].Name;
			std::cout << "Score: ";
			std::cin >> arr[i].Score;
			std::cout << std::endl;
		}
	}
};

void Sort(Player* arr, int NumberOfPlayers) {

	bool a = true;                                          //Изначально переменная a true
	while (a)												//Цикл будет выполняться, пока а true
	{
		a = false;                                         //При заходе в цикл переменной присваиватся значение false
		for (int i = 0; i < NumberOfPlayers - 1; i++)      //От количества игроков отнимается 1, потому что далее мы будем сравнивать счет i и i+1
		{
			if (arr[i].Score < arr[i + 1].Score)           //Если счет игрока слева меньше, чем счет игрока справа, то они меняются местами, 
			{                                              //и пременной a присвается значение true
				std::swap(arr[i], arr[i + 1]);             //Если условие if неверно, то а так и остается false, и дальше производится проверка счета следом стоящего игрока
				a = true;
			}
		}
	}
}

int main()
{
	int NumberOfPlayers;

	std::cout << "Enter Number Of Players: ";  //Узнаем колическтво игроков
	std::cin >> NumberOfPlayers;
	std::cout << std::endl;

	Player* arr = new Player[NumberOfPlayers];   //Создается динамический массив типа Player

	Player player;
	player.EnterPlayers(arr, NumberOfPlayers);    //Вызывается метод класса, в скобочках пишем название массива, куда сохраняются значения переменных, и количество игроков

	Sort(arr, NumberOfPlayers);                //Вызывается функция сортировки

	for (int i = 0; i < NumberOfPlayers; i++)  //Вызывается цикл, который выводит отсортированный список
	{
		std::cout << i + 1 << " " << arr[i].Name << '\t' << arr[i].Score << std::endl;
	}
	delete[] arr;

	return 0;

}

